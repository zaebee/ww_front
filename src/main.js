import Vue from 'vue'

import router from './router/index.js'
import api from './plugins/api'
import tools from './plugins/tools'
import store from './store'
import Cookie from 'vue-js-cookie'

Vue.use(api)
Vue.use(tools)
Vue.use(Cookie)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  created () {
    console.log('created')
  },
  router,
  store,
  // render: h => h(FeedPage)
})
