import Vue from 'vue'

import HTTP from '../http-common'
import { SET_ENV } from '../mutation-types'

let state = {
  google_map_key: 'AIzaSyAFzETh7AEn0R1AVSUQjq9TM05m8LGqWQo',
  facebook_app_id: '841314812711250',
  vkontakte_app_id: '5975951',
}


let getters = {}

let actions = {
  GET_ENV ({commit, state}) {
    let url = SET_ENV
    HTTP.get(url).then(
      response => {
        commit(SET_ENV, response.data)
      }
    )
  }
}

let mutations = {
  [SET_ENV] (state, payload) {
    state = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
