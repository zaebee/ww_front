
import HTTP from '../http-common'
import { SET_USER, SET_USER_FAILURE, CLEAR_USER } from '../mutation-types'
import { SIGNUP_USER, LOGIN_USER, SUBSCRIBE_USER } from '../mutation-types'

let state = {
  me: {},
  errors: {},
  language: 'ru',
  provider: null,
  authenticated: false
}

let getters = {
  profile_url: function () { console.log('state.me: ', state.me); return `/profiles/${state.me.id}/`}
}

let actions = {
  GET_USER ({commit, state}, provider) {
    let url = SET_USER
    return HTTP.get(url).then(
      response => {
        console.log('response.data: ', response.data)
        commit(url, response.data)
      },
      error => {
        commit(SET_USER_FAILURE, error.response.data)
      }
    )
  },
  LOGIN ({commit, state}, payload) {
    let url = LOGIN_USER
    return HTTP.post(url, payload).then(
      response => {
        let token = response.data.auth_token
        if (token) {
          response.data.user.token = token
        }
        commit(SET_USER, response.data)
      },
      error => {
        console.log(url, error.response.data)
        commit(SET_USER_FAILURE, error.response.data)
      }
    )
  },
  SIGNUP ({commit, state}, payload) {
    let url = SIGNUP_USER
    return HTTP.post(url, payload).then(
      response => {
        console.log(url, response.data)
        commit(SET_USER, response.data)
      },
      error => {
        console.log(url, error.response.data)
        commit(SET_USER_FAILURE, error.response.data)
      }
    )
  },
  LOGOUT ({commit, state}) {
    let url = CLEAR_USER
    return HTTP.get(url).then(
      response => {
        commit(CLEAR_USER)
      }
    )
  },
  SUBSCRIBE_USER ({commit, state}, payload) {
    let url = SUBSCRIBE_USER
    return HTTP.post(url, payload).then(
      response => {
        commit(SUBSCRIBE_USER)
      }
    )
  }
}

let mutations = {
  [SET_USER] (state, payload) {
    state.errors = {}
    state.me = payload
    state.provider = payload.provider
    state.authenticated = true
  },
  [SET_USER_FAILURE] (state, payload) {
    state.errors = payload
  },
  [CLEAR_USER] (state, payload) {
    state.me = {}
    state.cleaned = true
    state.provider = null
    state.authenticated = false
  },
  [SUBSCRIBE_USER] (state, payload) {
    state.errors = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
