import facebookLogin from 'facebook-login'
import vkontakte from 'vk-openapi'

import env from './env'
import user from './user'

import HTTP from '../http-common'
import { WW_AUTH, SET_USER_FAILURE } from '../mutation-types'

let state = {
  facebook: facebookLogin({appId: env.state.facebook_app_id}),
  vkontakte: new vkontakte.init({apiId: env.state.vkontakte_app_id})
}

let getters = {
  facebook: () => state.facebook,
  vkontakte: () => state.vkontakte,
  vk: () => vkontakte
}

let actions = {
  AUTH ({commit, state}, payload) {
    let url = WW_AUTH
    return HTTP.post(url, payload).then(
      response => {
        // console.log(url, response.data)
        // response.data.provider = payload.provider
        commit(WW_AUTH, response.data)
      },
      error => {
        // console.log(url, error.response.data)
        commit(SET_USER_FAILURE, error.response.data)
      }
    )
  },
  RESET_PASSWORD ({commit, state}, payload) {
    return HTTP.post('/auth/password/reset/', payload)
  }
}

let mutations = {
  [WW_AUTH] (state, payload) {
    user.state.errors = {}
    user.state.me = payload
    user.state.provider = payload.provider
    user.state.authenticated = true
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}
