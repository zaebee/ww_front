// import api from '../../components/'

import HTTP from '../http-common'
import { SET_CITY, SET_CATEGORIES } from '../mutation-types'

const state = {
  id: null,
  slug: '',
  name: '',
  title: '',
  content: '',
  country: '',
  description: '',
  cover_image: '',
  alt_names: {},
  location: {},
  titles: {},
  details: {},
  cover_image_opacity: null,
  popular_places: [],
  best_places: [],
  collections: [],
  events: [],
  categories: [],
  persons: [],
  events_count: 0,
  collections_count: 0
}

const getters = {
  get_id: state => state.id
}

const actions = {
  GET_CITY ({commit, state}, city_slug) {
    let url = `/cities/${city_slug}/`
    HTTP.get(url).then(
      response => {
        commit(SET_CITY, response.data)
      },
      error => {
        console.log('city error: ', error)
      }
    )
  }
}

const mutations = {
  [SET_CITY] (state, payload) {
    // change city stage here
    // Object.assign(state, payload._city)
    state.slug = payload.slug
    state.id = payload._city.id
    state.name = payload._city.name
    state.cover_image = payload._city.cover_image
    state.content = payload._city.content
    state.description = payload._city.description
    state.cover_image_opacity = payload._city.cover_image_opacity

    state.events_count = payload.counters.events.text
    state.collections_count = payload.counters.collections.text

    state.titles = payload.titles
    state.persons = payload.persons
    state.collections = payload.collections

    state.details = {
      events: {
        text: payload.counters.events.text,
        url: '/events/'
      },
      collections: {
        text: payload.counters.collections.text,
        url: '/collections/'
      }
    }

    state.best_places = payload.top_places
    state.popular_places = payload.popular_places
    state.events = [payload.day_events]

    let row = 0
    for(let category in payload.categories ){
      row = row === 4 ? 0 : row
      if(state.categories.length < 12){
        if(!state.categories[row]){
          state.categories[row] = []
        }
        state.categories[row].push(payload.categories[category])
      }
      row += 1
    }
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
