import HTTP from '../http-common'
import { FEED_EVENTS, FEED_FILTERS } from '../mutation-types'

const state = {
  filters: {
    cities: [],
    categories: []
  },
  events: [],
  total_events: 0,
  cities: [],
  categories: [],
  page: 0,
  next_page: true,
  openSettingsModal: false,
  month: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
  toggleSpinner: false
}

const getters = {}

const actions = {
  GET_FEED_EVENTS ({commit}) {
    if ((state.page && !!state.next_page) || (!state.page)) {
      state.page += 1
      state.toggleSpinner = true
      return HTTP.get(FEED_EVENTS + '?page=' + state.page).then(
        response => {
          commit(FEED_EVENTS, response.data)
          state.toggleSpinner = false
        },
        error => {
          console.log(FEED_EVENTS, error)
          state.toggleSpinner = false
        }
      )
    }
  },
  GET_FEED_FILTERS () {
    return HTTP.get(FEED_FILTERS).then(
      response => {
        state.cities = response.data.cities
        state.categories = response.data.categories
        state.filters = response.data.filters
        if (!response.data.filters.cities.length && !response.data.filters.categories.length) {
          state.openSettingsModal = true
        }
      },
      error => {
        console.log('GET_FEED_FILTERS: ', error)
      }
    )
  },
  UPDATE_FEED_FILTERS ({dispatch, state}, filters) {
    state.filters = filters

    HTTP.post(FEED_FILTERS, {
      cities: state.filters.cities,
      categories: state.filters.categories
    }).then(response => {
      state.page = 0
      state.events = []
      dispatch('GET_FEED_EVENTS')
    })
    console.log('UPDATE_FEED_FILTERS: ', state.filters)
  }
}

const mutations = {
  [FEED_EVENTS] (state, data) {
    state.events = state.events.concat(data.results)
    state.total_events = data.count
    state.next_page = data.next
  },

  [FEED_FILTERS] (filters) {
    state.filters = filters
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
