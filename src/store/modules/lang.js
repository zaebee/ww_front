
import HTTP from '../http-common'
import { SET_LANG } from '../mutation-types'

let state = {}

let getters = {}

let actions = {
  GET_LANG ({commit, state}) {
    let url = SET_LANG
    HTTP.post(url).then(
      response => {
        commit(SET_LANG, response.data)
      }
    )
  }
}

let mutations = {
  [SET_LANG] (state, payload) {
    state = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
