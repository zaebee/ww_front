import Vue from 'vue'
import Vuex from 'vuex'

import * as actions from './actions'

import env from './modules/env'
import user from './modules/user'
import social from './modules/auth'
import city from './modules/city'
import feed from './modules/feed'
import buckets from './modules/buckets'


Vue.use(Vuex)

export default new Vuex.Store({
  actions,
  modules: {
    env,
    user,
    social,
    city,
    feed,
    buckets,
  },
})
