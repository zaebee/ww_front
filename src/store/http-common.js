import axios from 'axios'

const baseURL = '/api'

const axiosConfig = {
  baseURL: baseURL,
  timeout: 30000,
  headers: {'X-Requested-With': 'XMLHttpRequest'},
  xsrfCookieName: 'csrftoken',
  xsrfHeaderName: 'X-CSRFToken'
}

const HTTP = axios.create(axiosConfig)
export default HTTP
