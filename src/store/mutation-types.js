export const SET_ENV = '/env/'

export const SET_LANG = '/i18n/setlang/'

export const WW_AUTH = '/auth/ww_auth/login/'
export const SET_USER = '/me/'

export const LOGIN_USER = '/auth/login/'
export const CLEAR_USER = '/auth/logout/'
export const SIGNUP_USER = '/auth/register/'
export const SUBSCRIBE_USER = '/auth/subscribe/'

export const SET_CITY = '/cities/:slug/'
export const SET_CATEGORIES = '/cities/:slug/:category/'

export const FEED_FILTERS = '/feed_filters/'
export const FEED_EVENTS = '/feed_search/'

// Failure states
export const SET_USER_FAILURE = 'SET_USER_FAILURE'

export const WEEK_BUCKET = '0:H-7d'
export const MONTH_BUCKET = '0:H-1M'
