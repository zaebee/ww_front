import Vue from 'vue'
import Router from 'vue-router'

import HomePage from '../pages/HomePage'
import FeedPage from '../pages/FeedPage'

Vue.use(Router)

const routes = [
  // {path: '/', name: 'mainpage', component: HomePage},
  {path: '/feed/', name: 'feed', component: FeedPage},
  {path: '/feed/:city/', name: 'feed-city', component: FeedPage},
  {path: '/feed/:category/', name: 'feed-category', component: FeedPage},
  {path: '/:city_slug/', name: 'city', component: HomePage}
]

const router = new Router({
  mode: 'history',
  routes: routes
})

export default router
