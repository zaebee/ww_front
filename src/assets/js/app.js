import $ from 'jquery';
import Swiper from 'swiper/dist/js/swiper';

$(document).ready(() => {

    const ANIMATE_SPEED = 350;

    $('.b-dropdown').each(function () {

        let dropdown = $(this),
            timeout;

        $('.js-dropdown-title', dropdown).on('click', function (event) {

            event.preventDefault();
            dropdown.toggleClass('active');

        });

        dropdown.on('mouseleave', () => {

            timeout = setTimeout(() => {

                dropdown.removeClass('active');

            }, 250);

        }).on('mouseenter', () => {

            clearTimeout(timeout);

        });

    });

    $('.b-tabs').each(function () {
        let tabs = $(this);

        $('.js-item', tabs).on('click', function (event) {
            event.preventDefault();

            let clicked_link = $(this),
                active_link = clicked_link.closest('.b-tabs__togglers').find('.active');

            if (!clicked_link.hasClass('active')) {

                let visible_tab = $(active_link.attr('href')),
                    next_tab = $(clicked_link.attr('href'));

                active_link.removeClass('active');
                clicked_link.addClass('active');

                visible_tab.removeClass('show');
                next_tab.addClass('show');

            }

        })

    });

    $('.b-header').each(function () {

        let header = $(this),
            start_scroll_position = header.height();

        $(window).on('scroll', function () {

            let scroll_height = $(this).scrollTop();

            if (!header.hasClass('floated') && scroll_height > start_scroll_position) {

                header.addClass('floated');


            } else if (header.hasClass('floated') && scroll_height < start_scroll_position) {

                header.removeClass('floated');

            }

        });

        $('.js-header-toggler', header).on('click', function (event) {

            event.preventDefault();
            $('.b-search', header).slideToggle(350);

        });

    });

    $('.b-modal__body').on('show', function () {

        let modal = $(this),
            bg = modal.closest('.b-modal');

        $('.b-modal__body:visible').hide();

        modal.fadeIn(ANIMATE_SPEED);
        bg.fadeIn(ANIMATE_SPEED);
        $(document.body).addClass('fixed');

    }).on('hide', function () {

        let modal = $(this),
            bg = modal.closest('.b-modal');

        modal.fadeOut(ANIMATE_SPEED);
        bg.fadeOut(ANIMATE_SPEED);
        $(document.body).removeClass('fixed');

    }).each(function () {

        let modal = $(this);

        $('.js-close-modal', modal).on('click', (event) => {

            event.preventDefault();
            modal.trigger('hide');

        });

    });

    $('.b-modal').on('click', (event) => {

        if (!$(event.target).closest('.b-modal__body').length) {
            $('.b-modal__body:visible').trigger('hide');
        }

    });

    $('.js-get-modal').on('click', function (event) {

        event.preventDefault();

        let link = $(this),
            target = link.data('target') || link.attr('href');

        $(target).trigger('show');


    });

    $('.js-like').on('click', function (event) {
        event.preventDefault();

        $(this).toggleClass('active');

    });

    $('.b-events').each(function () {

        let events = $(this),
            setSquareFormat = (parent) => {

                $('.b-events__img', parent).each(function () {

                    let img = $(this);
                    img.height(img.width());

                });

            };

        setSquareFormat(events);

        $(window).on('resize', () => {
            setSquareFormat(events);
        });


    });

    $('.b-filters').each(function () {

        let filters = $(this);
        $('.js-add-filter-item input', filters).on('focus', () => {
            $('.dd-list', filters).slideDown('150');
        }).on('blur', () => {
            $('.dd-list', filters).slideUp('150');
        });

    });

});
