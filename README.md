# WhatWhere Front
> This project uses [Vue.js](https://ru.vuejs.org/v2/guide/) as main framework
## Prepare Project, Ubuntu 16+
```
# install nodejs 8 
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# create work directory
cd ~/ && mkdir projects && cd projects

# get project repository
git clone -b develop https://github.com/alarstudios/whatwhere_front.git

# install vue-cli
npm install -g vue-cli

# install npm requirements
cd whatwhere_front && npm i
```
## Build Setup (From [webpack-simple](https://github.com/vuejs-templates/webpack-simple))

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
